#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

using namespace std;

int main() {

    const int N = 10;

    int Arr[N][N];

    time_t t;
    time(&t);

    int sum = 0;
    int k = (localtime(&t)->tm_mday) % N;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            Arr[i][j] = i + j;

            cout << Arr[i][j] << ' ';
        }
        cout << "\n";
    }

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (i == k-1) sum += Arr[i][j];
        }
    }

    cout << '\n' << "Sum elements of row " << k << " = " << sum << '\n';

    return 0;
}